# PyramidWordService

A small, Flask-based web service that determines if a string passed in the URL is a pyramid word.

# Requirements

* Python 3.7 or above
* Flask 1.1.1 or above
* Flask-API 2.0 or above

# How to Run

## Install dependencies.

If you haven't, already, run the following commands from your command line:

    pip install flask
    pip install flask-api


## Running the service

In your command line, navigate to the directory where you checked out this repository.
Then, execute the following command:

    python main.py

You should see something along the lines of the following:

    * Serving Flask app "main" (lazy loading)
    * Environment: production
      WARNING: This is a development server. Do not use it in a production deployment.
      Use a production WSGI server instead.
    * Debug mode: off
    * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

If you get an error like "No such file or command found: 'python'," then that means Python isn't
on your PATH.  Please refer to your operating system's documentation for how to edit the PATH
environment variable according to your chosen platform.

Once the service is running, open a new tab in your browser of choice (e.g. Firefox) and
navigate to the IP address (including port) on which Flask has reported as running.  You should
receive the following JSON ouput:

    {
        "status": "running"
    }

From there, you can test the service as you desire using URLs of the following format:

    http://127.0.0.1:5000/{word}

Where {word} is the word you wish to test.

If you, as an example, used the following URL:

    http://127.0.0.1:5000/banana

Your output JSON should look like:

    {
        "result": true
    }
