from collections import Counter
from flask_api   import FlaskAPI, exceptions

import json

app = FlaskAPI( __name__ )

def TriangleNumber( num: int ):
    '''
    TriangleNumber() function.

    Calculate the triangle number of the provided argument;
    a short form of the following pattern:

    n + ( n - 1 ) + ( n - 2 ) + ... + 1
    '''
    return ( num ** 2 + num ) // 2

@app.route( '/<string:word>', methods = ['GET'] )
def IsPyramidWord( word: str ):
    if ( word is None ):
        raise exceptions.NotAcceptable( 'Word is null.' )

    counts = Counter( word )

    # If the word is a pyramid word, then the
    #  sums of the counts should be the triangle
    #  number of the number of unique characters
    #  in the word.
    target  = TriangleNumber( len( counts ) )
    current = 0

    for char in counts:
        current += counts[char]

    return { 'result': current == target }

@app.route( '/' )
def Test():
    return { 'status': 'running' }

if ( __name__ == '__main__' ):
    app.run()
